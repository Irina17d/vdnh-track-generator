coordinates = [[2, 1], [4, 4], [7, 3], [10, 8], [12, 4]]
start_point = [0, 0]
now_point = start_point
answer = [start_point]
for _ in range(len(coordinates)):
    next_point = 0
    min_length = 0
    for i in coordinates:
        length = (i[0] - now_point[0]) ** 2 + (i[1] - now_point[1]) ** 2
        if min_length == 0 or length < min_length:
            min_length = length
            next_point = i
    answer.append(next_point)
    coordinates.remove(next_point)
    now_point = next_point
print(answer)
