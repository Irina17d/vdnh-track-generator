# *Документация по использованию API*
Ниже подробно описанны действия для запуска Backend
## *Настройка venv*
```sh
cd backend
python3 -m venv venv

venv\Scripts\activate.bat # для Windows;
source venv/bin/activate # для Linux и MacOS.
```

## *Установка зависимостей*
```sh
pip install --upgrade pip
pip install -r requirements.txt
```
## *Используемая БД*
В проекте настроено подключение к MongoDB в облаке. 
Дополнительных действий для настройки подключения к базе не требуется.

Полезные команды:
```sh
python manage.py makemigrations # создаем миграции для моделей 
# Миграции нужно делать каждый раз при изменении описания моделей
python manage.py migrate # затем миграции нужно применить к базе
python manage.py createsuperuser # создаем учетку с правами админа
python manage.py changepassword username # смена пароля пользователя
```

## *Запуск проекта*
```sh
python manage.py runserver
```
Swagger доступен по ссылке: http://127.0.0.1:8000/swagger

Также данные можно редактировать в админке: http://127.0.0.1:8000/admin

## *Замечания по модели данных*
Координаты точек интереса и точки в маршрутах хранятся в 
виде JSON объектов для совместимости с разными базами данных.
Валидаци этих объектов, на текущий момент, нет.
В поле можно положить любой JSON объект, но предполагается, что там будут массивы
координат и точек соответственно.
