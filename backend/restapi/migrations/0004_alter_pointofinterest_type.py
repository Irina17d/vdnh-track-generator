# Generated by Django 4.1.6 on 2023-02-14 14:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('restapi', '0003_pointofinterest_type_alter_pointofinterest_name_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pointofinterest',
            name='type',
            field=models.CharField(blank=True, choices=[('museum', 'Museum')], max_length=10),
        ),
    ]
