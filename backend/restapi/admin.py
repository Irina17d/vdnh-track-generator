from django.contrib import admin
from .models import PointOfInterest, Route


# Register your models here.

class PointOfInterestAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'description', 'coordinates', 'url', 'type', 'connections')
    list_display_links = ('id', 'name')
    search_fields = ('id', 'name', 'description', 'type')
    list_filter = ('name', 'type')


admin.site.register(PointOfInterest, PointOfInterestAdmin)


class RouteAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'description', 'points', 'duration', 'distance')
    list_display_links = ('id', 'name')
    search_fields = ('id', 'name', 'description')
    list_filter = ('name',)


admin.site.register(Route, RouteAdmin)