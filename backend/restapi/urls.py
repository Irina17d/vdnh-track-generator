from rest_framework import routers
from .api import PointOfInterestViewSet, RouteViewSet , ShortRoute


router = routers.DefaultRouter()
router.register('PointOfInterest', PointOfInterestViewSet, 'PointOfInterest')
router.register('Route', RouteViewSet, 'Route')
router.register('GetShertRoute', ShortRoute, 'GetShertRoute')


urlpatterns = router.urls
