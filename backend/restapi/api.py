from rest_framework import viewsets, permissions, mixins, status
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from .models import PointOfInterest, Route
from .serializers import PointOfInterestSerializer, RouteSerializer, PointOfInterestAllSerializer
import io
import json


class ShortRoute(viewsets.GenericViewSet, mixins.CreateModelMixin,):
    queryset = PointOfInterest.objects.all()
    
    serializer_class = PointOfInterestAllSerializer

    def create(self, request):
        user_data_string = JSONRenderer().render(request.data)
        user_data = io.BytesIO(user_data_string)
        user_id_points = json.loads(user_data_string)

        points_full = PointOfInterestAllSerializer(
            self.queryset, many=True)  # все точки из базы
        start_point = points_full.data[-1]

        user_route = [start_point]

        # составляется список(user_route) словарей (точек) пользовательского маршрута
        for indexID in range(len(user_id_points)):
            for indexFull in range(len(points_full.data)):
                if points_full.data[indexFull]['id'] == user_id_points[indexID]:
                    user_route.append(points_full.data[indexFull])

        # удаляем лишнии связи из route['connection']
        for route in user_route:
            list_of_points = []
            for connection in route['connections']:
                if connection["pointId"] in user_id_points:
                    list_of_points.append(connection)
            route['connections'] = list_of_points

        short_route = [start_point]
        black_list = [start_point["id"]]
        now_point = start_point

        for _ in range(len(user_route) - 1):
            list_of_distance = []

            for connection in now_point['connections']:
                if connection["pointId"] not in black_list:
                    list_of_distance.append(connection["distance"])

            flag = False
            min_length = min(list_of_distance)
            for connection in now_point['connections']:
                if min_length == connection["distance"]:
                    print(connection['pointId'])
                    for point_user_route in user_route:
                        if point_user_route['id'] == connection['pointId']:
                            short_route.append(point_user_route)
                            black_list.append(point_user_route['id'])
                            flag = True
                            break
                if flag:
                    break
            now_point = point_user_route

        data = json.loads(JSONRenderer().render(short_route))
        return Response(data, status=status.HTTP_201_CREATED)


class PointOfInterestViewSet(viewsets.ModelViewSet):
    queryset = PointOfInterest.objects.all()
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly
    ]
    serializer_class = PointOfInterestSerializer


class RouteViewSet(viewsets.ModelViewSet):
    queryset = Route.objects.all()
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly
    ]
    serializer_class = RouteSerializer
