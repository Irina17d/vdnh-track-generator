from django.db import models


# Create your models here.

class PointOfInterest(models.Model):
    Types = (
        ("Museum", "Museum"),
        ("Food", "Food"),
        ("Entertainment", "Entertainment"),
        ("Fountain", "Fountain")
    )
    name = models.CharField(max_length=64)
    description = models.CharField(max_length=2000, blank=True)
    coordinates = models.JSONField(default=dict)
    url = models.CharField(max_length=100, blank=True)
    type = models.CharField(blank=True, default="Museum", choices=Types, max_length=15)
    connections = models.JSONField( blank=True)


class Route(models.Model):
    name = models.CharField(max_length=64)
    description = models.CharField(max_length=2000, blank=True)
    url = models.CharField(max_length=50, blank=True)
    points = models.JSONField(default=dict)
    duration = models.CharField(max_length=50, blank=True)
    distance = models.CharField(max_length=50, blank=True)
