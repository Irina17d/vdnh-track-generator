from rest_framework import serializers
from .models import PointOfInterest, Route


class PointOfInterestSerializer(serializers.ModelSerializer):
    class Meta:
        model = PointOfInterest
        fields =  ['id', 'name', 'description', 'coordinates', 'url', 'type']

class PointOfInterestAllSerializer(serializers.ModelSerializer):
    class Meta:
        model = PointOfInterest
        fields =  '__all__'
        
class RouteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Route
        fields = '__all__'
