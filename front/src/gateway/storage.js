const PREFIX = "VDNH"


export function setUserRoute(route) { localStorage.setItem(`${PREFIX}_userRoutes`, JSON.stringify([route])) }
export async function addUserRoute(route) {
    const userRoutes = await getUserRoutes() || []
    userRoutes.push(route)
    localStorage.setItem(`${PREFIX}_userRoutes`, JSON.stringify(userRoutes))
}

export async function getUserRoutes() { return JSON.parse(localStorage.getItem(`${PREFIX}_userRoutes`)) }


export function deleteUserRoute(route) {
    // логика описывающие удаление маршрута из localStorage.VDNH_userRoutes
}

export function clearUserRoutes() {
    localStorage.clear(`${PREFIX}_userRoutes`)
}

export function setToken(token) {
    localStorage.setItem(`${PREFIX}_token`, JSON.stringify(token))
}
export function getLocaltocken() {
    return JSON.parse(localStorage.getItem(`${PREFIX}_token`))
}
export function clearToken() {
    localStorage.clear(`${PREFIX}_token`)
}
export function setlogin(login) {
    localStorage.setItem(`${PREFIX}_login`, JSON.stringify(login))
}
export function getLogin() {
    return JSON.parse(localStorage.getItem(`${PREFIX}_login`))
}
export function clearLogin() {
    localStorage.clear(`${PREFIX}_login`)
}