import { setToken, getLocaltocken, setlogin } from "./storage";
import toRoutes from '../domain/toRoutes'
const API_URL = import.meta.env.VITE_API_URL;

const headers = {
    Authorization: "Bearer " + getLocaltocken(),
    'Content-Type': 'application/json;charset=utf-8'
}

const get = {
    method: "get",
};
const post = {
    method: "post",
    headers,
    body: {},
};
const put = {
    method: "put",
    headers,
    body: {},
};


export async function getPoints() {
    const res = await fetch(`${API_URL}/api/PointOfInterest/`, get)
    const points = await res.json()
    return points
}
export async function addPoint(point) {
    post.body = JSON.stringify(point)
    const res = await fetch(`${API_URL}/api/PointOfInterest/`, post)
    const points = await res.json()
    return points
}

export async function updatePoint(point, id) {
    put.body = JSON.stringify({
        name: point.name,
        coordinates: point.coordinates,
        description: point.description,
        url: point.url,
        type: point.type,
        connections: point.connections
    })
    const res = await fetch(`${API_URL}/api/PointOfInterest/${id}/`, put)
    const pointRes = await res.json()
    return pointRes
}

export async function getToken(options) {
    post.body = JSON.stringify({ username: options.login, password: options.password })
    const res = await fetch(`${API_URL}/api/token/`, post)
    const token = await res.json()
    setToken(token.access)
    setlogin(options.login)
    headers.Authorization = `Bearer  ${token.access}`
    return token.access
}


export async function getRoutes() {
    const res = await fetch(`${API_URL}/api/Route/`, get)
    const routes = await res.json()
    return toRoutes(routes)
}
export async function getRoute(pointsId) {
    console.log(pointsId);
    post.body = JSON.stringify(pointsId)
    post.headers = { 'Content-Type': 'application/json;charset=utf-8' }
    const res = await fetch(`${API_URL}/api/GetShertRoute/`, post)
    const route = await res.json()
    return route
}

export async function addStaticRoute(route) {
    post.body = JSON.stringify({
        name: route.name,
        description: route.description,
        url: route.url,
        points: route.points,
        duration: route.duration,
        distance: route.distance
    })
    const res = await fetch(`${API_URL}/api/Route/`, post)
    const routes = await res.json()
    return routes
}
export async function updateRoute(route, id) {
    put.body = JSON.stringify({
        name: route.name,
        description: route.description,
        url: route.url,
        points: route.points,
        duration: route.duration,
        distance: route.distance
    })
    const res = await fetch(`${API_URL}/api/Route/${id}/`, put)
    const routes = await res.json()
    return routes
}

