import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import YmapPlugin, { loadYmap } from "vue-yandex-maps";

const settings = {
  apiKey: 'dc4cee08-3ea7-4bf0-8d03-282f90f40888', // Индивидуальный ключ API
  lang: 'ru_RU', // Используемый язык
  coordorder: 'latlong', // Порядок задания географических координат
  debug: false, // Режим отладки
  version: '2.1' // Версия Я.Карт
}



async function start() {
  await loadYmap(settings);
}
start()

const app = createApp(App)

app.use(YmapPlugin, settings).mount('#app')


