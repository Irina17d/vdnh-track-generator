export default function toRoutes(serverRoutes) {
    const routes = []

    serverRoutes.forEach(serverRoute => {
        routes.push(toRoute(serverRoute))
    });
    return routes
}

function toRoute(route) {
    return {
        id: route.id,
        name: route.name,
        duration: route.duration || 'неизвестно',
        distance: route.distance || 'неизвестно',
        url: route.url,
        points: route.points,
        description: route.description,
    }
}